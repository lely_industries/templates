# Templates project

[[_TOC_]]

This projects [`.gitlab-ci.yml`] file also serves as an example.

## [Component]s

### [artifactory]

Provides access tokens for Artifactory. Modifies the [build-image component] to use Artifactory for
storing images.

```yml
include:
    - component: $CI_SERVER_FQDN/$CI_PROJECT_PATH/artifactory@$1.0.0
      inputs:
          version: 1.0.0
```

This [component] provides several [hidden jobs] that can be [extend]ed.

#### `.Artifactory Authenticate`

Generates a job and project scoped access token.

```yaml
extends:
    - .Artifactory Authenticate
```

- The token has generic read access to all projects and write access to the root group of the
  project.

    note that this is broader than desired. Which is write access to only the project.

- For these projects the token has the same roles as a developer.
- The token will be revoked after the job timeout.

#### `.Artifactory netrc`

Creates `/dev/shm/.netrc` which is for example expected to be there by several base images.

- If your job has its own [script] then use a [reference tag] to add this in addition to
  [extend]ing:

    ```yaml
    extends:
        - .Artifactory netrc
    script:
        - !reference [.Artifactory netrc, script]
    ```

- Use the following in combination with cURL or other tools supporting a `.netrc` file:

    ```sh
    ln --symbolic "/dev/shm/.netrc" "${HOME}/.netrc"
    ```

#### `.Artifactory jf config`

Sets all variables used by the JFrog CLI to the correct values for this job.

- If your job has its own [script] then use a [reference tag] to add this in addition to
  [extend]ing:

    ```yaml
    extends:
        - .Artifactory jf config
    script:
        - !reference [.Artifactory jf config, script]
    ```

#### `.Artifactory Publish job`

Publishes the job so that the artifacts can be bundled, promoted and released.

- If your job has its own [script] then use a [reference tag] to add this in addition to
  [extend]ing:

    ```yaml
    extends:
        - .Artifactory Publish job
    script:
        - !reference [.Artifactory Publish job, script]
    ```

#### `.Artifactory Publish pipeline`

Publishes the pipeline to bundle the artifacts, preparing them for promotion or release.

When building a tag, the artifacts are promoted to `TST` from `DEV`.

- If your job has its own [script] then use a [reference tag] to add this in addition to
  [extend]ing:

    ```yaml
    extends:
        - .Artifactory Publish pipeline
    script:
        - !reference [.Artifactory Publish pipeline, script]
    ```

- All your published jobs are [need]ed:

    ```yaml
    Artifactory Publish pipeline
    extends:
        - .Artifactory Publish pipeline
    needs:
        - Build image "podman-jfrog"
        - Build image ""
    ```

#### `version` and `version_prefix` parameters

[component]s requires you to set the version as the `version` and `version_prefix` parameters if
they use these internally. For example to reference the correct image that comes with the component.
(Which unfortunately is code duplication.)

```yaml
include:
    - component: $CI_SERVER_FQDN/$CI_PROJECT_PATH/artifactory@1.0.0
      inputs:
          version: 1.0.0
```

```yaml
include:
    - component: $CI_SERVER_FQDN/$CI_PROJECT_PATH/artifactory@branche_reference
      inputs:
          version: branche_reference
          version_prefix: ${DIR_REF}
```

#### parameters

| Input                | Default value                                                               | Description                                                                                          |
| -------------------- | --------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------- |
| `version`            | `latest`                                                                    | Repeat the version of the component.                                                                 |
| `version_prefix`     | `${DIR_TAG}`                                                                | The prefix for the version.                                                                          |
| `podman_jfrog_image` | `${ARTIFACTORY_DOMAIN}/${CM_ROOT}-docker/${CM_PATH}/templates/podman-jfrog` | Advanced setting: The image to use for building images.                                              |
| `ARTIFACTORY_DOMAIN` | `${ARTIFACTORY_DOMAIN}`                                                     | Advanced setting: The domain name of the Artifactory server.                                         |
| `JFROG_ENV_EXCLUDE`  | `password;secret;key;token;auth;JWT`                                        | Advanced setting: See <https://docs.jfrog-applications.jfrog.io/jfrog-applications/jfrog-cli/usage>. |
| `JFROG_FAIL_NO_OP`   | `true`                                                                      | Advanced setting: See <https://docs.jfrog-applications.jfrog.io/jfrog-applications/jfrog-cli/usage>. |
| `JFROG_LOG_LEVEL`    | `WARN`                                                                      | Advanced setting: See <https://docs.jfrog-applications.jfrog.io/jfrog-applications/jfrog-cli/usage>. |
| `VAULT_AUTH_ROLE`    | `gitlab-ci`                                                                 | Advanced setting: See <https://docs.gitlab.com/ee/ci/secrets/#configure-your-vault-server>.          |
| `VAULT_DOMAIN`       | `${VAULT_DOMAIN}`                                                           | Advanced setting: The domain name of the HashiCorp Vault server.                                     |

### build-image

By default the [build-image component] builds the [Dockerfile] in the `docker` directory at the root
of your project and uploads it to the default docker registry. It also produces an environment
variable artifact `CS_IMAGE` with the image name. To use the image in subsequent jobs, [extend]
these jobs with `.Use image ""`:

```yaml
default:
    interruptible: true

include:
    - component: $CI_SERVER_FQDN/$CI_PROJECT_PATH/build-image@1.0.0
      inputs:
          version: 1.0.0

YourJob:
    extends:
        - .Use image ""
    script: |
        printf 'Dockerfile: %s' "${CS_DOCKERFILE_PATH}"
        printf 'image (build): %s' "${CS_IMAGE}"
        printf 'image (used) : %s' "${CI_JOB_IMAGE}"
```

This is the minimum you need and should be sufficient in most use cases.

#### `version` and `version_prefix` parameters

[component]s requires you to set the version as the `version` and `version_prefix` parameters if
they use these internally. For example to reference the correct image that comes with the component.
(Which unfortunately is code duplication.)

```yaml
include:
    - component: $CI_SERVER_FQDN/$CI_PROJECT_PATH/build-image@1.0.0
      inputs:
          version: 1.0.0
```

```yaml
include:
    - component: $CI_SERVER_FQDN/$CI_PROJECT_PATH/build-image@branche_reference
          version: branche_reference
          version_prefix: ${DIR_REF}
```

#### Building multiple Images

If your project needs more than one image, you can include this [component] multiple times. Each
time with another value for `image_dir` or `image_variant`.

- `image_dir` allows you to have other `Dockerfile`s in sub-directories.
    - Note the use of the `image_dir` in `.Use image "YourImage"` in the example below.
    - The variable `IMAGE_DIR` is available in jobs extended with `.Use image "..."` with the value
      of `image_dir`.
- `image_variant` allows you to build variants of a `Dockerfile`.
    - The variant is passed to the `Dockerfile` as the `IMAGE_VARIANT` ARG.
    - The variable `IMAGE_VARIANT` is available in jobs extended with `.Use image "..."` with the
      value of `image_variant`.
- The `Dockerfile` and image used are also available in jobs extended with `.Use image "..."` as
  `CS_DOCKERFILE_PATH` and `CS_IMAGE` respectively.

```yaml
default:
    interruptible: true

include:
    - component: $CI_SERVER_FQDN/$CI_PROJECT_PATH/build-image@1.0.0
      inputs:
          version: 1.0.0
    - component: $CI_SERVER_FQDN/$CI_PROJECT_PATH/build-image@1.0.0
      inputs:
          version: 1.0.0
          image_dir: Special
          image_variant: Yours

YourJob:
    extends:
        - .Use image ""
    script: |
        printf 'image_dir: %s' "${IMAGE_DIR}"
        printf 'image_variant: %s' "${IMAGE_VARIANT}"
        printf 'Dockerfile: %s' "${CS_DOCKERFILE_PATH}"
        printf 'image (build): %s' "${CS_IMAGE}"
        printf 'image (used) : %s' "${CI_JOB_IMAGE}"

YourSpecialJob:
    extends:
        - .Use image "Special"
    script: |
        printf 'image_dir: %s' "${IMAGE_DIR}"
        printf 'image_variant: %s' "${IMAGE_VARIANT}"
        printf 'Dockerfile: %s' "${CS_DOCKERFILE_PATH}"
        printf 'image (build): %s' "${CS_IMAGE}"
        printf 'image (used) : %s' "${CI_JOB_IMAGE}"

Artifactory Publish pipeline:
    needs:
        - Build image ""
        - Build image "Special"
```

See the [images] project for an advanced example with [matrix].

#### parameters

| Input                   | Default value                                                               | Description                                                                                                                                     |
| ----------------------- | --------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------- |
| `version`               | `latest`                                                                    | Repeat the version of the component.                                                                                                            |
| `version_prefix`        | `${DIR_TAG}`                                                                | The prefix for the version.                                                                                                                     |
| `podman_jfrog_image`    | `${ARTIFACTORY_DOMAIN}/${CM_ROOT}-docker/${CM_PATH}/templates/podman-jfrog` | Advanced setting: The image to use for building images.                                                                                         |
| `image_dir`             |                                                                             | The subdirectory in `${DIR_DOCKERFILE}` there the `Dockerfile` can be found.                                                                    |
| `image_variant`         |                                                                             | The variant of the `Dockerfile` to be build. The value is passed in `${IMAGE_VARIANT}` as `ARG` to the build.                                   |
| `CS_IMAGE_SUFFIX`       |                                                                             | Advanced setting: Enable fips mode for the container scanner.                                                                                   |
| `DIR_CACHE`             | `/cache`                                                                    | Advanced setting: The directory to store the build cache. Prefixed with a `/` if it is not empty.                                               |
| `DIR_COMMIT`            | `/commit`                                                                   | Advanced setting: The directory to store the images with commit sha versions (`CI_COMMIT_SHA`). Prefixed with a `/` if it is not empty.         |
| `DIR_DOCKERFILE`        | `docker`                                                                    | Advanced setting: The base directory of the `Dockerfile`.                                                                                       |
| `DIR_REF`               | `/ref`                                                                      | Advanced setting: The directory to store the images with branch refs versions (`CI_COMMIT_REF`). Prefixed with a `/` if it is not empty.        |
| `DIR_TAG`               |                                                                             | Advanced setting: The directory to store the images with branch refs versions (`CI_COMMIT_TAG`). Prefixed with a `/` if it is not empty.        |
| `DOCKERFILE_BUILD_ARGS` |                                                                             | Advanced setting, unstable interface: A white-space separated array of build arguments passed with the build command. (`--build-arg=arg=value`) |
| `DOCKERFILE_SECRETS`    |                                                                             | Advanced setting, unstable interface: A white-space separated array of build secrets passed with the build command. (`--secret=id=id,src=path`) |

### hadolint

Scans your `Dockerfile`s for code quality issues.

```yaml
include:
    - component: $CI_SERVER_FQDN/$CI_PROJECT_PATH/hadolint@1.0.0
```

## Templates

In general, you start your '.gitlab-ci.yml' file with templates. These have a fixed structure. Here
is a stripped example:

```yml
default:
    interruptible: true

include:
    - project: $CM_TEMPLATES
      ref: 1.0.0
      file:
          - template/... # list of templates
    - template: ... # list of Gitlab templates
    - component: ...
      inputs: ...
    - local: ...
```

There are two template sections. The first are templates from this project (named `template`). The
second are [Gitlab templates].

Some of these templates have parameters which are passed as variables in the subsequent `variables:`
section. [Component]s are configured using their (optional) inputs.

Note that the `ref:` version is the same as the component versions for this project. (Unavoidable
code duplication.)

Most [Gitlab templates] have a [component] counterpart. Components are better in several ways and
should be used instead of templates unless you can explain why.

### PyLint

See: <https://gitlab.lelyonline.com/common/cm/pylint>

### caching

Unmaintained. Do not use unless you can explain why.

### precommit

See: <https://gitlab.lelyonline.com/common/cm/precommit>

### tox

Requires an update: <https://gitlab.lelyonline.com/common/cm/templates/-/issues/31>

Automates python unit testing.

Example: <https://gitlab.lelyonline.com/common/cm/gitlabmoveitem/-/blob/main/.gitlab-ci.yml#L11>

## Images

### podman-jfrog

This image contains [podman] and [jfrog-cli] and is setup to use the `.netrc` file provided by the
[artifactory component].

The image is used by the [build-image component].

---

[artifactory]: https://artifactory.lelyonline.com/
[artifactory component]: #artifactory
[build-image component]: #build-image
[component]: https://docs.gitlab.com/ee/ci/components/
[dockerfile]: https://docs.docker.com/reference/dockerfile/
[extend]: https://docs.gitlab.com/ee/ci/yaml/#extends
[gitlab templates]:
    https://docs.gitlab.com/ee/user/application_security/#security-scanning-without-auto-devops
[hidden jobs]: https://docs.gitlab.com/ee/ci/jobs/index.html#hide-a-job
[images]: https://gitlab.lelyonline.com/common/cm/images/
[jfrog-cli]: https://docs.jfrog-applications.jfrog.io/jfrog-applications/jfrog-cli/
[matrix]: https://docs.gitlab.com/ee/ci/yaml/#parallelmatrix
[need]: https://docs.gitlab.com/ee/ci/yaml/#needs
[podman]: https://podman.io/
[reference tag]: https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#reference-tags
[script]: https://docs.gitlab.com/ee/ci/yaml/#script
[`.gitlab-ci.yml`]:
    https://gitlab.lelyonline.com/common/cm/templates/-/blob/46-update-documentation-for-components/.gitlab-ci.yml

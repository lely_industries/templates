spec:
    inputs:
        version:
            default: latest
            description: >
                Repeat the version of the component.
        version_prefix:
            default: ${DIR_TAG}
            options:
                - ${DIR_COMMIT}
                - ${DIR_REF}
                - ${DIR_TAG}
            description: >
                The prefix for the version.
        image_dir:
            default: ""
            description: >
                The subdirectory in `${DIR_DOCKERFILE}` there the `Dockerfile` can be found.
        image_variant:
            default: ""
            description: >
                The variant of the `Dockerfile` to be build. The value is passed in
                `${IMAGE_VARIANT}` as `ARG` to the build.
        podman_jfrog_image:
            default: ${CI_REGISTRY}/${CM_ROOT}-docker/${CM_PATH}/templates/podman-jfrog
            description: >
                Advanced setting: The image to use for building images.
        CS_IMAGE_SUFFIX:
            default: ""
            options:
                - ""
                - -fips
            description: >
                Advanced setting: Enable fips mode for the container scanner.
        DIR_CACHE:
            default: /cache
            description: >
                Advanced setting: The directory to store the build cache. Prefixed with a `/` if it
                is not empty.
        DIR_COMMIT:
            default: /commit
            description: >
                Advanced setting: The directory to store the images with commit sha versions
                (`CI_COMMIT_SHA`). Prefixed with a `/` if it is not empty.
        DIR_DOCKERFILE:
            default: docker
            description: >
                Advanced setting: The base directory of the `Dockerfile`.
        DIR_REF:
            default: /ref
            description: >
                Advanced setting: The directory to store the images with branch refs versions
                (`CI_COMMIT_REF`). Prefixed with a `/` if it is not empty.
        DIR_TAG:
            default: ""
            description: >
                Advanced setting: The directory to store the images with branch refs versions
                (`CI_COMMIT_TAG`). Prefixed with a `/` if it is not empty.
        DOCKERFILE_BUILD_ARGS:
            default: ""
            description: >
                Advanced setting, unstable interface: A white-space separated array of build
                arguments passed with the build command. (`--build-arg=arg=value`)
        DOCKERFILE_SECRETS:
            default: ""
            description: >
                Advanced setting, unstable interface: A white-space separated array of build
                secrets passed with the build command. (`--secret=id=id,src=path`)
---
# These global variables are unavoidable, as these are inputs for `$[[ inputs.version_prefix ]]`
variables:
    DIR_CACHE: $[[ inputs.DIR_CACHE ]]
    DIR_COMMIT: $[[ inputs.DIR_COMMIT ]]
    DIR_REF: $[[ inputs.DIR_REF ]]
    DIR_TAG: $[[ inputs.DIR_TAG ]]

include:
    - component: $CI_SERVER_FQDN/${CM_PATH}/templates/artifactory@$[[ inputs.version ]]
      inputs:
          version: $[[ inputs.version ]]
          version_prefix: $[[ inputs.version_prefix ]]
          podman_jfrog_image: $[[ inputs.podman_jfrog_image ]]
    - component: $CI_SERVER_FQDN/components/container-scanning/container-scanning@4.2.0
      inputs:
          cs_image: ${CS_IMAGE}
          git_strategy: fetch
          job_name: .container_scanning

.Build image:
    extends:
        - .Artifactory Publish job
    needs: []
    script:
        - |
            # Prepare feedback
            PodmanTrace() { (
                set -x
                podman "$@"
            ); }
            # Prepare Artifactory
        - !reference [.Artifactory jf config, script]
        - !reference [.Artifactory netrc, script]
        - |
            # Sanitize input
            while read -r DIR; do
                declare "${DIR}=/${!DIR#/}"
                declare "${DIR}=${!DIR%/}"
            done <<EOF
            DIR_CACHE
            DIR_COMMIT
            DIR_DOCKERFILE
            DIR_REF
            DIR_TAG
            IMAGE_DIR
            EOF

            CI_REGISTRY_IMAGE="${CI_REGISTRY_IMAGE%/}"
            IMAGE_VARIANT="${IMAGE_VARIANT%/}"
            BASE_URL="${CI_REGISTRY_IMAGE}${IMAGE_DIR}${IMAGE_VARIANT}"
            CS_DOCKERFILE_PATH="${DIR_DOCKERFILE}${IMAGE_DIR}/Dockerfile"
            CS_DOCKERFILE_PATH="${CS_DOCKERFILE_PATH#/}"
            CS_IMAGE="${BASE_URL}${DIR_COMMIT}:${CI_COMMIT_SHA}"

            # Build the following image:
            printf 'CS_DOCKERFILE_PATH=%s\n' "${CS_DOCKERFILE_PATH}" | tee --append -- dotenv.env
            printf 'CS_IMAGE=%s\n' "${CS_IMAGE}" | tee --append -- dotenv.env

            # Build args
            DOCKERFILE_BUILD_ARGS=(${DOCKERFILE_BUILD_ARGS:+${DOCKERFILE_BUILD_ARGS}})
            if grep --fixed-strings --quiet -- "CI_COMMIT_SHA" "${CS_DOCKERFILE_PATH}"; then
                DOCKERFILE_BUILD_ARGS+=("--build-arg=CI_COMMIT_SHA=${CI_COMMIT_SHA}")
            fi
            if [[ -n ${IMAGE_VARIANT} ]]; then
                DOCKERFILE_BUILD_ARGS+=("--build-arg=IMAGE_VARIANT=${IMAGE_VARIANT}")
            fi

            # Build cache
            CACHE="${BASE_URL}${DIR_CACHE}"

            # Build secrets
            DOCKERFILE_SECRETS=(${DOCKERFILE_SECRETS:+${DOCKERFILE_SECRETS}})
            if [[ -f "/dev/shm/.netrc" ]]; then
                DOCKERFILE_SECRETS+=("--secret=id=.netrc,src=/dev/shm/.netrc")
            fi

            # registries login
            head -c -1 <<EOF | PodmanTrace login \
                --password-stdin \
                --username="${CI_REGISTRY_USER}" \
                "${CI_REGISTRY}"
            ${CI_REGISTRY_PASSWORD}
            EOF

            # Build the image
            if ! printf '%s\0' "${DOCKERFILE_BUILD_ARGS[@]}" |
                grep --fixed-strings --null --quiet -- "--no-cache" &&
                podman pull --quiet "${CS_IMAGE}" 2>/dev/null; then
                printf 'Not rebuilding: %s\n' "${CS_IMAGE}"
            else
                PodmanTrace build \
                    "${DOCKERFILE_BUILD_ARGS[@]}" \
                    --cache-from="${CACHE}/${CI_DEFAULT_BRANCH}" \
                    --cache-from="${CACHE}/${CI_COMMIT_REF_SLUG}" \
                    --cache-to="${CACHE}/${CI_COMMIT_REF_SLUG}" \
                    --file="${CS_DOCKERFILE_PATH}" \
                    "${DOCKERFILE_SECRETS[@]}" \
                    --tag="${CS_IMAGE}" \
                    .
            fi
            podman image inspect --format '{{ .Digest }}' "${CS_IMAGE}"

            # Push the images
            PodmanTrace push "${CS_IMAGE}"
            if [[ -z ${CI_COMMIT_TAG-} ]]; then
                ALTERNATE_TAG="${BASE_URL}${DIR_REF}:${CI_COMMIT_REF_SLUG}"
            else
                ALTERNATE_TAG="${BASE_URL}${DIR_TAG}:${CI_COMMIT_TAG}"
            fi
            podman tag "${CS_IMAGE}" "${ALTERNATE_TAG}"
            PodmanTrace push "${ALTERNATE_TAG}"
            if [[ ${CI_COMMIT_BRANCH-} == "${CI_DEFAULT_BRANCH}" ]]; then
                ALTERNATE_TAG="${BASE_URL}${DIR_TAG}:latest"
                podman tag "${CS_IMAGE}" "${ALTERNATE_TAG}"
                PodmanTrace push "${ALTERNATE_TAG}"
            fi

            # Publish the job
        - !reference [.Artifactory Publish job, script]
    artifacts:
        paths:
            - dotenv.env
        reports:
            dotenv: dotenv.env

.Scan image:
    # Would like to use `extends: .container_scanning`,
    # but need to exclude `dependencies`, `stage` and `rules` sections.
    # Would like to use `variables: !reference [.container_scanning, variables]`,
    # but this erases all other variables sections.
    extends:
        - .Artifactory Authenticate
    variables:
        CS_SCHEMA_MODEL: 15
        GIT_STRATEGY: fetch
    image: !reference [.container_scanning, image]
    script: !reference [.container_scanning, script]
    allow_failure: !reference [.container_scanning, allow_failure]
    artifacts: !reference [.container_scanning, artifacts]

Build image "$[[ inputs.image_dir ]]$[[ inputs.image_variant ]]":
    extends:
        - .Build image
    variables:
        IMAGE_DIR: $[[ inputs.image_dir ]]
        IMAGE_VARIANT: $[[ inputs.image_variant ]]
        DOCKERFILE_BUILD_ARGS: $[[ inputs.DOCKERFILE_BUILD_ARGS ]]
        DOCKERFILE_SECRETS: $[[ inputs.DOCKERFILE_SECRETS ]]
        DIR_DOCKERFILE: $[[ inputs.DIR_DOCKERFILE ]]
    image: $[[ inputs.podman_jfrog_image ]]$[[ inputs.version_prefix ]]:$[[ inputs.version ]]

.Use image "$[[ inputs.image_dir ]]$[[ inputs.image_variant ]]":
    needs:
        - Build image "$[[ inputs.image_dir ]]$[[ inputs.image_variant ]]"
    variables:
        IMAGE_DIR: $[[ inputs.image_dir ]]
        IMAGE_VARIANT: $[[ inputs.image_variant ]]
    image: ${CS_IMAGE}

Scan image "$[[ inputs.image_dir ]]$[[ inputs.image_variant ]]":
    extends:
        - .Scan image
    needs:
        - Build image "$[[ inputs.image_dir ]]$[[ inputs.image_variant ]]"
    variables:
        CS_IMAGE_SUFFIX: $[[ inputs.CS_IMAGE_SUFFIX ]]

Artifactory Publish pipeline:
    extends:
        - .Artifactory Publish pipeline
    needs:
        - Build image "$[[ inputs.image_dir ]]$[[ inputs.image_variant ]]"

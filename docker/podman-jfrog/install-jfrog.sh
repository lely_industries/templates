#!/usr/bin/env sh
set -eu

: "${JFROG_VERSION:="$1-"}"
: "${JFROG_CLI_AVOID_NEW_VERSION_WARNING:="TRUE"}"
export JFROG_CLI_AVOID_NEW_VERSION_WARNING
CURL="curl --fail --globoff --location --netrc --remote-name --show-error --silent"
JFROG_REPO="https://releases.jfrog.io/artifactory/jfrog-cli"
SCRIPT="install-cli.sh"

# Try downloading installation script from artifactory.
if [ -n "${ARTIFACTORY_DOMAIN-}" ]; then
    ARTIFACTORY_REPO="https://${ARTIFACTORY_DOMAIN}/artifactory/jfrogiocli-generic-remote"
    if ${CURL} "${ARTIFACTORY_REPO}/v2-jf/scripts/${SCRIPT}"; then
        sed --in-place "s#${JFROG_REPO}#${ARTIFACTORY_REPO}#g" "${SCRIPT}"
    fi
fi

# Try downloading installation script from jfrog.
if [ ! -f "${SCRIPT}" ]; then
    ${CURL} "${JFROG_REPO}/v2-jf/scripts/${SCRIPT}"
fi

# Sanitize installation script
sed --in-place "s#^curl .*\$#${CURL} \"\${URL}\"#g" "${SCRIPT}"
sed --in-place '/jf intro/d' "${SCRIPT}"
chmod +x "${SCRIPT}"

# Execute installation script.
if [ -n "${JFROG_VERSION}" ]; then
    "./${SCRIPT}" "${JFROG_VERSION}"
else
    "./${SCRIPT}"
fi
rm --force -- "${SCRIPT}"

# Backwards compatibility with 'jfrog' script name.
JFROG="$(command -v jf)"
ln --symbolic "${JFROG}" "$(dirname -- "${JFROG}")/jfrog"

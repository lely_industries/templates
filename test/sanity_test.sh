#!/usr/bin/env sh
set -eu

CURL="curl --fail-with-body --globoff --location --silent"

get() {
    RESPONSE="$(${CURL} "$1")"
    ERROR=$?
    set -e
    if [ "${ERROR}" -ne 0 ]; then
        jq '.' 1>&2 <<EOF
${RESPONSE}
EOF
        printf 'Project "%s" will not be found with insufficient access rights.\n' "${CI_PROJECT_ID}" 1>&2
    fi
    return "${ERROR}"
}

# Prepare for local testing.
if [ -z "${CI:+x}" ]; then
    : "${CI_API_V4_URL="https://gitlab.lelyonline.com/api/v4"}"
    DOMAIN="${CI_API_V4_URL##*"://"}"
    DOMAIN="${DOMAIN%%[:/]*}"
    if [ -r "${HOME}/.netrc" ]; then
        #
        PYTHON="$(command -v python3 || command -v python || command -v py || true)"
        if [ -n "${PYTHON}" ]; then
            PRIVATE_TOKEN="$("${PYTHON}" -)" <<EOF
import netrc
item=netrc.netrc().authenticators("${DOMAIN}")
item and print(item[2])
EOF
            CURL="${CURL} --header PRIVATE-TOKEN:${PRIVATE_TOKEN}"
        fi
    fi

    if [ -z "${CI_PROJECT_ID:+x}" ]; then
        CI_PROJECT_ID="$(git -C "$(dirname -- "$0")" ls-remote --get-url 2>/dev/null)"
        CI_PROJECT_ID="${CI_PROJECT_ID%".git"}"
        CI_PROJECT_ID="${CI_PROJECT_ID#*"${DOMAIN}"[:/]}"
        CI_PROJECT_ID="$(
            jq --arg PROJECT "${CI_PROJECT_ID}" --null-input --raw-output '$PROJECT | @uri'
        )"
    fi

    if [ -z "${CI_PIPELINE_ID:+x}" ]; then
        COMMIT_REF_NAME="$(git -C "$(dirname -- "$0")" branch --show-current 2>/dev/null)"
        ROUTE="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/commits/${COMMIT_REF_NAME}"
        if get "${ROUTE}"; then
            CI_PIPELINE_ID="$(jq '.last_pipeline.id')" <<EOF
${RESPONSE}
EOF
        fi
    fi

    if [ -z "${CI_PROJECT_DESCRIPTION:+x}" ]; then
        ROUTE="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}"
        if get "${ROUTE}"; then
            CI_PROJECT_DESCRIPTION="$(jq --raw-output '.description')" <<EOF
${RESPONSE}
EOF
        fi
    fi
else
    CURL="${CURL} --header JOB-TOKEN:${CI_JOB_TOKEN-}"
fi

# The actual tests.
RESULT=0

# Ensure that the desired jobs are created.
ROUTE="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/jobs"
if get "${ROUTE}"; then
    while read -r JOB; do
        COUNT="$(jq --arg JOB "${JOB}" 'map(.name) | indices($JOB) | length')" <<EOF
${RESPONSE}
EOF
        if [ "${COUNT}" -ne 1 ]; then
            RESULT=$((RESULT | 1))
            printf 'Missing job %s.\n' "${JOB}"
        else
            printf 'Job %s exists.\n' "${JOB}"
        fi
    done <<EOF
Build image ""
Scan image ""
Build image "podman-jfrog"
Scan image "podman-jfrog"
Artifactory Publish pipeline
EOF
fi

RESULT=$((RESULT >> 1))

# Ensure that a project description exists, because it will be important to display the resource in
# the catalog.
if [ -z "${CI_PROJECT_DESCRIPTION}" ]; then
    RESULT=$((RESULT | 1))
    printf 'Please set the projet description.\n'
else
    printf 'Project description set.\n%s\n' "${CI_PROJECT_DESCRIPTION}"
fi

RESULT=$((RESULT >> 1))

# Ensure that a `README.md` exists in the root directory as it represents the documentation for the
# whole components repository.
FILENAME="README.md"
if [ -f "${FILENAME}" ]; then
    printf '%s file exist.\n' "${FILENAME}"
else
    RESULT=$((RESULT | 1))
    printf 'Please add a %s file.\n' "${FILENAME}"
fi

exit "${RESULT}"

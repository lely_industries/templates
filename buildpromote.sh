#!/usr/bin/env bash
set -Eeuo pipefail
shopt -s inherit_errexit

# This file is created for debug purposes. It will do exactly the same as
# `Build image.gitlab-ci.yml` and `Artifactory.gitlab-ci.yml`. But then locally.
# You can compare the content of this file with the others and verify that these are equivalent.
# At some point in the future, this file will probably removed.
# The section `Prepare CI` sets up the runner environment with proper local variables.
# Acting as if we are adding a tag using the branch name is intentional to achieve full test
# coverage. Note that this of course will add records to the production instance of Artifactory.
# Be prepared to remove everything that will not be removed automatically by the
# garbage collector of Artifactory, particularly the promoted release bundle in `TEST`.

cleanup() {
    printf 'Cleanup\n'
    jf rt build-clean
    jf config remove --quiet
    rm --force -- dotenv.env /dev/shm/.netrc
    export -n CI
    export -n JFROG_CLI_AVOID_NEW_VERSION_WARNING
    export -n JFROG_CLI_FAIL_NO_OP
    export -n JFROG_CLI_LOG_LEVEL
    export -n JFROG_CLI_LOG_TIMESTAMP
    export -n JFROG_CLI_OFFER_CONFIG
    export -n JFROG_CLI_REPORT_USAGE
    export -n JFROG_CLI_BUILD_NAME
    export -n JFROG_CLI_BUILD_NUMBER
    export -n JFROG_CLI_BUILD_PROJECT
    export -n JFROG_CLI_BUILD_URL
    export -n JFROG_CLI_ENV_EXCLUDE
}

trap cleanup EXIT

read_netrc() {
    python3 - <<EOF
import netrc
item=netrc.netrc().authenticators("${CI_REGISTRY}")
item and print(item[$1])
EOF
}

sluggify() {
    python3 - <<EOF
import re
print(re.sub(r"(\W|_)", "-", "$1").strip("-").casefold()[:62])
EOF
}

# Instance variables
ARTIFACTORY_DOMAIN="artifactory.lelyonline.com"
# VAULT_DOMAIN="vault.lelyonline.com"
CM_ROOT="common"
CM_PATH="${CM_ROOT}/cm"

# Predefined variables
export CI=true
CI_COMMIT_REF_NAME="$(git branch --show-current 2>/dev/null || printf 'local\n')"
CI_COMMIT_TAG="${CI_COMMIT_REF_NAME}"
CI_COMMIT_SHA="$(git rev-list --max-count=1 HEAD -- 2>/dev/null || printf '0\n')"
CI_DEFAULT_BRANCH="main"
CI_JOB_ID="Job-${RANDOM}"
CI_JOB_NAME="local"
CI_JOB_URL="https://localhost"
CI_PIPELINE_ID="Pipeline-${RANDOM}"
# CI_PIPELINE_NAME="local"
CI_PIPELINE_URL="https://localhost"
CI_PROJECT_PATH="$(git ls-remote --get-url 2>/dev/null || basename -- "$(pwd || true)")"
CI_PROJECT_PATH="${CI_PROJECT_PATH##*:}"
CI_PROJECT_PATH="${CI_PROJECT_PATH%.git}"
CI_PROJECT_ROOT_NAMESPACE="${CI_PROJECT_PATH%%/*}"
# CI_PROJECT_NAME="${CI_PROJECT_PATH##*/}"

CI_COMMIT_REF_SLUG="$(sluggify "${CI_COMMIT_REF_NAME}")"
CI_JOB_NAME_SLUG="$(sluggify "${CI_JOB_NAME}")"
CI_PROJECT_PATH_SLUG="$(sluggify "${CI_PROJECT_PATH}")"

# spec:
#     inputs:
#         component_version:
# ARTIFACTORY_DOMAIN="${ARTIFACTORY_DOMAIN}"
JFROG_ENV_EXCLUDE="password;secret;key;token;auth;JWT"
JFROG_FAIL_NO_OP="true"
JFROG_LOG_LEVEL="WARN"
PODMAN_JFROG_IMAGE="${ARTIFACTORY_DOMAIN}/${CM_ROOT}-docker/${CM_PATH}/templates/podman-jfrog"
# VAULT_AUTH_ROLE="gitlab-ci"
# VAULT_DOMAIN="${VAULT_DOMAIN}"

# .Artifactory Authenticate:
CI_REGISTRY="${ARTIFACTORY_DOMAIN}"
CI_REGISTRY_IMAGE="${CI_REGISTRY}/${CI_PROJECT_ROOT_NAMESPACE}-docker/${CI_PROJECT_PATH}"
# VAULT_AUTH_ROLE="${VAULT_AUTH_ROLE}"
# VAULT_SERVER_URL="https://${VAULT_DOMAIN}"
# VAULT_ID_TOKEN="${VAULT_SERVER_URL}"
ARTIFACTORY_USER="$(read_netrc 0)"
CI_REGISTRY_USER="${ARTIFACTORY_USER}"
ARTIFACTORY_PASSWORD="$(read_netrc 2)"
CI_REGISTRY_PASSWORD="${ARTIFACTORY_PASSWORD}"

ArtifactoryNetrc() {
    # secrets/netrc file
    cat >>/dev/shm/.netrc <<EOF
machine ${ARTIFACTORY_DOMAIN}
login ${ARTIFACTORY_USER}
password ${ARTIFACTORY_PASSWORD}
EOF
}

# .Artifactory jf config:
export JFROG_CLI_AVOID_NEW_VERSION_WARNING="true"
export JFROG_CLI_FAIL_NO_OP="${JFROG_FAIL_NO_OP}"
export JFROG_CLI_LOG_LEVEL="${JFROG_LOG_LEVEL}"
export JFROG_CLI_LOG_TIMESTAMP="OFF"
export JFROG_CLI_OFFER_CONFIG="false"
export JFROG_CLI_REPORT_USAGE="false"

export JFROG_CLI_BUILD_NAME="${CI_PROJECT_PATH_SLUG}:${CI_JOB_NAME_SLUG}"
export JFROG_CLI_BUILD_NUMBER="${CI_JOB_ID}"
export JFROG_CLI_BUILD_PROJECT="${CI_PROJECT_ROOT_NAMESPACE}"
export JFROG_CLI_BUILD_URL="${CI_JOB_URL}"
export JFROG_CLI_ENV_EXCLUDE="${JFROG_ENV_EXCLUDE}"

ArtifactoryJFConfig() {
    # Update feedback
    CopyFunction() {
        local FUNCTION
        FUNCTION="$(declare -f "$1" || printf '%s () { :; }\n' "$1")"
        [[ -z $2 ]] || eval "${FUNCTION/$1/$2}"
    }
    CopyFunction PodmanTrace ArtifactoryPodmanTrace
    PodmanTrace() { (
        if [[ $1 == push || $1 == pull ]]; then
            set -x
            jf rt "podman-$1" "${@:2}" "" --module "$2" --skip-login
        else
            ArtifactoryPodmanTrace "$@"
        fi
    ); }

    # Configure JFrog CLI
    JFROG_CLI_BUILD_NAME="$(
        printf '%s\n' "${JFROG_CLI_BUILD_NAME}" | tr --squeeze-repeats "-"
    )"
    head -c -1 <<EOF | jf config add \
        --interactive=false \
        --overwrite \
        --password-stdin=true \
        --url="https://${ARTIFACTORY_DOMAIN}" \
        --user="${ARTIFACTORY_USER}"
${ARTIFACTORY_PASSWORD}
EOF
    jf rt build-clean
    jf rt build-add-git
    jf rt build-collect-env
}

ArtifactoryPublishJob() {
    # Publish the job
    jf rt build-publish
    printf 'JFROG_CLI_BUILD_NUMBER_%s=%s\n' \
        "${CI_JOB_NAME_SLUG//-/_}" "${JFROG_CLI_BUILD_NUMBER}" >>dotenv.env
}

ArtifactoryPublishPipeline() {
    export JFROG_CLI_BUILD_NAME="${CI_PROJECT_PATH_SLUG}"
    export JFROG_CLI_BUILD_NUMBER="${CI_PIPELINE_ID}"
    export JFROG_CLI_BUILD_URL="${CI_PIPELINE_URL}"
    printf 'CI_JOB_IMAGE=%s\n' "${PODMAN_JFROG_IMAGE}${PODMAN_JFROG_IMAGE_VERSION}"

    # Prepare Artifactory
    ArtifactoryJFConfig

    # Sanity check
    # There must be at least one job defined
    if [[ -z ${!JFROG_CLI_BUILD_NUMBER_*} ]]; then
        printf 'There are no jobs to publish.\n'
        exit 0
    fi

    # Aggregate jobs
    for JOB_NAME in "${!JFROG_CLI_BUILD_NUMBER_@}"; do
        (
            JOB_NAME_SLUG="${JOB_NAME#JFROG_CLI_BUILD_NUMBER_}"
            JOB_NAME_SLUG="${JOB_NAME_SLUG//_/-}"
            JOB_NAME_SLUG="$(
                printf '%s\n' "${JOB_NAME_SLUG}" | tr --squeeze-repeats "-"
            )"
            set -x
            jf rt build-append "${JFROG_CLI_BUILD_NAME}" "${JFROG_CLI_BUILD_NUMBER}" \
                "${CI_PROJECT_PATH_SLUG}:${JOB_NAME_SLUG}" "${!JOB_NAME}"
        )
    done

    # Publish the pipeline
    jf rt build-publish

    # Promote the pipeline artifacts
    jf release-bundle-delete-local --sync "${JFROG_CLI_BUILD_NAME}" "${CI_COMMIT_REF_NAME::32}" \
        2>/dev/null || true

    if [[ -n ${CI_COMMIT_TAG-} ]]; then
        FILE_SPEC="$(mktemp)"

        printf '{
            "files": [
                {
                    "build": "%s/%s",
                    "includeDeps": "false",
                    "project": "%s"
                }
            ]
        }' \
            "${JFROG_CLI_BUILD_NAME}" "${JFROG_CLI_BUILD_NUMBER}" "${JFROG_CLI_BUILD_PROJECT}" \
            >|"${FILE_SPEC}"

        jf release-bundle-create --signing-key="lely-industries" --spec="${FILE_SPEC}" --sync \
            "${JFROG_CLI_BUILD_NAME}" "${CI_COMMIT_REF_NAME::32}"
        rm --force -- "${FILE_SPEC}"
        jf release-bundle-promote --signing-key="lely-industries" --sync \
            "${JFROG_CLI_BUILD_NAME}" "${CI_COMMIT_REF_NAME::32}" TEST
    fi
}

# spec:
#     inputs:
#         component_version:
#         image_dir:
#         image_variant:
# CS_IMAGE_SUFFIX=""
DIR_CACHE="cache"
DIR_COMMIT="commit"
DIR_DOCKERFILE="docker"
DIR_REF="ref"
DIR_TAG=""
DOCKERFILE_BUILD_ARGS=""
DOCKERFILE_SECRETS=""
PODMAN_JFROG_IMAGE="${CI_REGISTRY}/${CM_ROOT}-docker/${CM_PATH}/templates/podman-jfrog"

# Job variables
IMAGE_DIR=""
IMAGE_VARIANT=""
PODMAN_JFROG_IMAGE_VERSION="/${DIR_REF}:${CI_COMMIT_REF_NAME}"

printf 'CI_JOB_IMAGE=%s\n' "${PODMAN_JFROG_IMAGE}${PODMAN_JFROG_IMAGE_VERSION}"

# Prepare feedback
PodmanTrace() { (
    set -x
    podman "$@"
); }

# Prepare Artifactory
ArtifactoryJFConfig
ArtifactoryNetrc

# Sanitize input
while read -r DIR; do
    declare "${DIR}=/${!DIR#/}"
    declare "${DIR}=${!DIR%/}"
done <<EOF
DIR_CACHE
DIR_COMMIT
DIR_DOCKERFILE
DIR_REF
DIR_TAG
IMAGE_DIR
EOF

CI_REGISTRY_IMAGE="${CI_REGISTRY_IMAGE%/}"
IMAGE_VARIANT="${IMAGE_VARIANT%/}"
BASE_URL="${CI_REGISTRY_IMAGE}${IMAGE_DIR}${IMAGE_VARIANT}"
CS_DOCKERFILE_PATH="${DIR_DOCKERFILE}${IMAGE_DIR}/Dockerfile"
CS_DOCKERFILE_PATH="${CS_DOCKERFILE_PATH#/}"
CS_IMAGE="${BASE_URL}${DIR_COMMIT}:${CI_COMMIT_SHA}"

# Build the following image:
printf 'CS_DOCKERFILE_PATH=%s\n' "${CS_DOCKERFILE_PATH}" | tee --append -- dotenv.env
printf 'CS_IMAGE=%s\n' "${CS_IMAGE}" | tee --append -- dotenv.env

# Build args
DOCKERFILE_BUILD_ARGS=(${DOCKERFILE_BUILD_ARGS:+${DOCKERFILE_BUILD_ARGS}})
if grep --fixed-strings --quiet -- "CI_COMMIT_SHA" "${CS_DOCKERFILE_PATH}"; then
    DOCKERFILE_BUILD_ARGS+=("--build-arg=CI_COMMIT_SHA=${CI_COMMIT_SHA}")
fi
if [[ -n ${IMAGE_VARIANT} ]]; then
    DOCKERFILE_BUILD_ARGS+=("--build-arg=IMAGE_VARIANT=${IMAGE_VARIANT}")
fi

# Build cache
CACHE="${BASE_URL}${DIR_CACHE}"

# Build secrets
DOCKERFILE_SECRETS=(${DOCKERFILE_SECRETS:+${DOCKERFILE_SECRETS}})
if [[ -f "/dev/shm/.netrc" ]]; then
    DOCKERFILE_SECRETS+=("--secret=id=.netrc,src=/dev/shm/.netrc")
fi

# registries login
head -c -1 <<EOF | PodmanTrace login \
    --password-stdin \
    --username="${CI_REGISTRY_USER}" \
    "${CI_REGISTRY}"
${CI_REGISTRY_PASSWORD}
EOF

# Build the image
if ! printf '%s\0' "${DOCKERFILE_BUILD_ARGS[@]}" |
    grep --fixed-strings --null --quiet -- "--no-cache" &&
    podman pull --quiet "${CS_IMAGE}" 2>/dev/null; then
    printf 'Not rebuilding: %s\n' "${CS_IMAGE}"
else
    PodmanTrace build \
        "${DOCKERFILE_BUILD_ARGS[@]}" \
        --cache-from="${CACHE}/${CI_DEFAULT_BRANCH}" \
        --cache-from="${CACHE}/${CI_COMMIT_REF_SLUG}" \
        --file="${CS_DOCKERFILE_PATH}" \
        "${DOCKERFILE_SECRETS[@]}" \
        --tag="${CS_IMAGE}" \
        .
fi
podman image inspect --format '{{ .Digest }}' "${CS_IMAGE}"

# Push the images
PodmanTrace push "${CS_IMAGE}"
if [[ -z ${CI_COMMIT_TAG-} ]]; then
    ALTERNATE_TAG="${BASE_URL}${DIR_REF}:${CI_COMMIT_REF_SLUG}"
else
    ALTERNATE_TAG="${BASE_URL}${DIR_TAG}:${CI_COMMIT_TAG}"
fi
podman tag "${CS_IMAGE}" "${ALTERNATE_TAG}"
PodmanTrace push "${ALTERNATE_TAG}"
if [[ ${CI_COMMIT_BRANCH-} == "${CI_DEFAULT_BRANCH}" ]]; then
    ALTERNATE_TAG="${BASE_URL}${DIR_TAG}:latest"
    podman tag "${CS_IMAGE}" "${ALTERNATE_TAG}"
    PodmanTrace push "${ALTERNATE_TAG}"
fi

# Publish the job
ArtifactoryPublishJob

# Publish the pipeline
. dotenv.env
ArtifactoryPublishPipeline
